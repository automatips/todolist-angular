angular.module('todoService', [])

	.factory('Todos', ['$http',function($http) {
		return {
			get : function() {
				return $http.get('/api/todos');
			},
			create : function(todoData) {
				return $http.post('/api/todos', todoData);
			},
			delete : function(id) {
				return $http.delete('/api/todos/' + id);
			},
			edit : function(todoData) {
				return $http.post('/api/todos', todoData);
			},
			complete : function(id) {
				return $http.complete('/api/todos/' + id);
			},
		}
	}]);