var mongoose = require('mongoose');

module.exports = mongoose.model('Todo', {
    task: {
        type: String,
        default: ''
    },
    status: {
        type: String,
        default: 'pending'
    },
    created: {
        type: Date,
        default: Date.now()
    },
    due: {
        type: Date,
        default: Date.now()
    }
});